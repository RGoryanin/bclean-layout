/*
 * For tests purpose. Remove on production
 */

$('.header__login-btn').on('click', (e) => {
    e.preventDefault();
    bcleanStore.setValue('user', {firstName: 'Иван', lastName: 'Иванов', id: '123'});
    bcleanStore.setValue('notifications', [
        {title: 'Notification 1', content: 'Content of notification 1'},
        {title: 'Notification 2', content: 'Content of notification 2'},
    ]);
});