function BcleanStore() {

    this.user = {
        id: '123456',
        firstName: 'Иван',
        lastName: 'Иванов',
    };
    this.notifications = [
        {title: 'Notification 1', content: 'Content of notification 1'},
        {title: 'Notification 2', content: 'Content of notification 2'},
    ]
    
    this.setValue = (field, value) => {
        this[field] = value;
        PubSub.publish(field, value)
    }

    this.subscribe = (field, cb) => {
        PubSub.subscribe(field, () => {
            cb(this[field]);
        });
    }

}

window.bcleanStore = new BcleanStore();
$(document).ready(() => {
  $(".phone__input").inputmask("+7 (999) 999-9999");
  $(".calculator-form-details-area").inputmask("9{1,4}кв");
})
//=require components/slider.js
/*============= Main Page Accordion Start ===============*/
const title = document.querySelectorAll('.index-faq__item-header');
const content = document.querySelectorAll('.index-faq__item-content');

for (let i = 0; i < title.length; i++) {
  title[i].addEventListener('click', function () {
    if (!(this.classList.contains('active'))) {
      for (let i = 0; i < title.length; i++) {
        title[i].classList.remove('active')
      }
      this.classList.add('active')
    } else {
      this.classList.remove('active')
    }
  })
}
/* ============== Main Page Accordion End =================*/



/*============= Accordion Start ===============*/
const initAccordion = () => {
  const $accordionsList = $('.accordion');

  [...$accordionsList].forEach(accordions => {
    const $headersList = $(accordions).find('.accordion__header');
    const $itemsList = $(accordions).find('.accordion__item');
    const $contentsList = $(accordions).find('.accordion__content');

    [...$headersList].forEach((header, index) => $(header).click(() => {
      const $item = $itemsList[index];

      if (!($item.classList.contains('accordion__item_active'))) {
        $item.classList.add('accordion__item_active')
      } else {
        $item.classList.remove('accordion__item_active')
      }
    }))

  })
};

initAccordion();

// const settingsTitle = document.querySelectorAll('.settings__accordion-header');
// const settingsContent = document.querySelectorAll('.settings__accordion-content');

// for(let i = 0; i < settingsTitle.length; i++) {
//   settingsTitle[i].addEventListener('click', function() {
//     if (!(this.classList.contains('active'))) {
//       this.classList.add('active')
//     } else {
//       this.classList.remove('active')
//     }
//   })
// }
/* ============== Accordion End =================*/

$('.clients__slider').magnificPopup({
  delegate: 'a', // child items selector, by clicking on it popup will open
  type: 'image',
  // other options
});


MicroModal.init();
/*======== TABS START========== */
let tab = function () {
  let tabNav = document.querySelectorAll('.types__tabs-item'),
      tabContent = document.querySelectorAll('.tab'),
      tabName;

  tabNav.forEach(item => {
    item.addEventListener('click', selectTabNav)
  });

  function selectTabNav() {

    tabNav.forEach(item => {
      item.classList.remove('is-active');
    });
    this.classList.add('is-active');

    tabName = this.getAttribute('data-tab-name');
    selectTabContent(tabName);
  }

  function selectTabContent() {
    tabContent.forEach(item => {
      if (item.classList.contains(tabName)) {
        $('.types__slider').slick('refresh');
        item.classList.add('is-active')
      } else {
        item.classList.remove('is-active')
      }
    })
  }
};

tab();
/*======== TABS END========== */

// REGISTRATION MODAL TABS

const initModalTabs = () => {
  const regBtn = $('.header__modal-reg-hdr')[0];
  const enterBtn = $('.header__modal-enter-hdr')[0];
  const regContent = $('.header__modal-registration')[0];
  const enterContent = $('.header__modal-enter')[0];

  $(regBtn).click(() => {
    regContent.classList.remove('d-none');
    enterContent.classList.add('d-none');

    regBtn.classList.add('color-blue');
    enterBtn.classList.remove('color-blue');
  });

  $(enterBtn).click(() => {
    enterContent.classList.remove('d-none');
    regContent.classList.add('d-none');

    regBtn.classList.remove('color-blue');
    enterBtn.classList.add('color-blue');
  });
};

initModalTabs();

// REGISTRATION MODAL TABS END

/*======== TOOLTIPS START========== */


/*======== TOOLTIPS END========== */

const initBars = () => {
  const bars = $('.stars-bar');

  [...bars].forEach(bar => {
    const value = $(bar).data('rating-value') || 0;
    $(bar).stars({ stars: 5, value, enabled: false })
  });
};

initBars();

const initClosedTestimonials = () => {
  const testimonials = $(".index-testimonials__block_gray");

  [...testimonials].forEach((testimonial) =>
    $(testimonial).click(() => {
      if (
        $(testimonial).hasClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        )
      ) {
        $(testimonial).removeClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        );
      } else {
        $(testimonial).addClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        );
      }
    })
  );
};

initClosedTestimonials();

// Header expand
$('.header__burger').on('click', function () {
    $('.header').addClass('header_opened')
})
$('.header__close').on('click', function () {
    $('.header').removeClass('header_opened')
});

// Header accordion
(function () {
    const title = document.querySelectorAll('.header-accordion__item-header');
    const content = document.querySelectorAll('.header-accordion__item-content');

    for (let i = 0; i < title.length; i++) {
        title[i].addEventListener('click', function () {
            if (!(this.classList.contains('active'))) {
                for (let i = 0; i < title.length; i++) {
                    title[i].classList.remove('active')
                }
                this.classList.add('active')
            }
        })
    }
})();

// Header tabs
(function () {
    let tabNav = document.querySelectorAll('.header__tab-btn'),
        tabContent = document.querySelectorAll('.header__tab'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove('bclean-tabs-vertical__tab_active');
        });
        this.classList.add('bclean-tabs-vertical__tab_active');

        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }

    function selectTabContent() {
        tabContent.forEach(item => {
            if (item.dataset.name === tabName) {
                item.classList.add('header__dropdown-content_active')
            } else {
                item.classList.remove('header__dropdown-content_active')
            }
        })
    }
})();

// Header switch content desktop
$('.header__link-residential').on('click', function ({ currentTarget }) {
    if (
        $('.header__dropdown-content[data-name="residential"]').hasClass('header__dropdown-content_active')
        && $('.header').hasClass('header_opened')
    ) {
        $('.header__dropdown-content[data-name="residential"]').removeClass('header__dropdown-content_active');
        $('.header').removeClass('header_opened');
        currentTarget.classList.remove('header__link_active')
        return;
    }
    [...$('.header__link')].forEach(link => link.classList.remove('header__link_active'))
    currentTarget.classList.add('header__link_active')
    $('.header__dropdown').addClass('header__dropdown_active');
    $('.header').addClass('header_opened');
    $('.header__dropdown-content').removeClass('header__dropdown-content_active');
    $('.header__dropdown-content[data-name="residential"]').addClass('header__dropdown-content_active');
});
$('.header__link-non-residential').on('click', function ({ currentTarget }) {
    if (
        $('.header__dropdown-content[data-name="non-residential"]').hasClass('header__dropdown-content_active')
        && $('.header').hasClass('header_opened')
    ) {
        $('.header__dropdown-content[data-name="non-residential"]').removeClass('header__dropdown-content_active');
        $('.header').removeClass('header_opened');
        currentTarget.classList.remove('header__link_active')
        return;
    }
    [...$('.header__link')].forEach(link => link.classList.remove('header__link_active'))
    currentTarget.classList.add('header__link_active')
    $('.header__dropdown').addClass('header__dropdown_active');
    $('.header').addClass('header_opened');
    $('.header__dropdown-content').removeClass('header__dropdown-content_active');
    $('.header__dropdown-content[data-name="non-residential"]').addClass('header__dropdown-content_active');
});
$(document).on('click', function (e) {
    if ($('.header').hasClass('header_opened') && !e.target.closest('.header')) {
        $('.header').removeClass('header_opened');
    }
});

// Header data
$(document).ready(userHandler);
$(document).ready(notifiactionsHandler);

function userHandler() {
    bcleanStore.subscribe('user', (user) => {
        if (!user) {
            $('.header__user-name').text(null);
            $('.header').removeClass('is-logged-in');
            $('body').removeClass('is-logged-in');
        } else {
            $('.header__user-name').text(user.lastName + ' ' + user.firstName);
            $('.header').addClass('is-logged-in');
            $('body').addClass('is-logged-in');
        }
    });
    $('.header__user-name').text(bcleanStore.user.lastName + ' ' + bcleanStore.user.firstName);

    $('.header__logout-btn').on('click', (e) => {
        e.preventDefault();
        bcleanStore.setValue('user', null);
    });
}

function notifiactionsHandler() {
    bcleanStore.subscribe('notifications', (notifications) => {
        if (notifications && notifications.length) {
            $('.header').addClass('has-notifications');
        } else {
            $('.header').removeClass('has-notifications');
        }
    });

    $('.header_notification-btn').on('click', (e) => {
        e.preventDefault();
        // show notifications popup window
    });
}

/**
 * Services and types list items selection
 */
(() => {
    const servicesMap = [
        [0, 1, 2],
        [1, 2, 3],
        [3, 5, 6],
        [1, 4, 5],
        [0, 1, 7],
        [3, 6, 8],
    ];

    $('.header__types-list-residential > li').on('click', (e) => {
        $('.header__types-list-residential').addClass('active');
        $('.header__services-list-residential').addClass('active');
        if (!e.target.classList.contains('.active')) {
            const services = $('.header__services-list-residential li');
            services.removeClass('active');
            $('.header__types-list-residential > li').removeClass('active');
            e.target.classList.add('active');
            const index = $('.header__types-list-residential > li').toArray().indexOf(e.target);
            const servicesIds = servicesMap[index];
            services.each((i, item) => {
                if (servicesIds.includes(i)) {
                    item.classList.add('active');
                }
            });
        }
    });
})();

(() => {
    const types = $('.header .header__type');

    [...types].forEach(type => $(type).on('click', () => {
        if (type.classList.contains('header__type-active')) {
            type.classList.remove('header__type-active');
        } else {
            type.classList.add('header__type-active');
        }
    }))
})();
/* Tab switching on types page */
(function () {
    var tabNav = document.querySelectorAll('.types-page .types-page__tabs-item'),
        tabContent = document.querySelectorAll('.types-page .types-page__tab-content'),
        tabName;
  
    tabNav.forEach(item => {
      item.addEventListener('click', selectTabNav)
    });
  
    function selectTabNav() {
      tabNav.forEach(item => {
        item.classList.remove('is-active');
      });
      this.classList.add('is-active');
  
      tabName = this.getAttribute('data-tab-name');
      selectTabContent(tabName);
    }
  
    function selectTabContent() {
      tabContent.forEach(item => {
        if (item.dataset.tabId === tabName) {
          item.classList.add('is-active')
        } else {
          item.classList.remove('is-active')
        }
      })
    }
  })();
const initJobsPage = () => {
    $('.current-vacancies-card').on('click', (e) => {
        const card = e.target.closest('.current-vacancies-card');
        if (card.dataset.page) {
            window.location.href = card.dataset.page;
        }
    });

    $('.jobs-page__send-cv-btn').on('click', (e) => {
        $('.job-apply-overlay').addClass('is-opened');
        $('.job-apply-modal').addClass('is-opened');
    });

    $('.job-apply-overlay').on('click', (e) => {
        if (e.target.classList.contains('job-apply-overlay')) {
            $('.job-apply-overlay').removeClass('is-opened');
            $('.job-apply-modal').removeClass('is-opened');
        }
    });
};

const jobsData = [
    {
        title: 'Клинер для уборки офисов',
        sub1: 'Клининг',
        sub2: 'Москва',
        sub3: 'Полный день'
    },
    {
        title: 'Клинер для уборки квартир',
        sub1: 'Клининг',
        sub2: 'Москва',
        sub3: 'Полный день'
    },
    {
        title: 'Клинер 1',
        sub1: 'Клининг',
        sub2: 'Москва',
        sub3: 'Полный день'
    },
    {
        title: 'Клинер 2',
        sub1: 'Клининг',
        sub2: 'Москва',
        sub3: 'Полный день'
    }
];

const initJobs = (data = jobsData) => {
    const mappedJobs = data.map(({ title, sub1, sub2, sub3 }) => `
        <div class="col-xl-4">
            <div class="current-vacancies-card" data-page="/job-desc.html">
                <h1>${title}</h1>
                <div>
                    <span>${sub1}</span>
                    <span>${sub2}</span>
                    <span>${sub3}</span>
                </div>
            </div>
        </div>
    `);

    mappedJobs.push(`
        <div class="col-xl-4">
            <div class="current-vacancies-card-apply" data-page="/job-desc.html">
                <h1>Нет подходящей вакансии?</h1>
                <button class="bclean-btn bclean-btn_white-blue jobs-page__send-cv-btn">Отправить резюме</button>
            </div>
        </div>
    `);

    const jobsContainer = $('.current-vacancies-list');

    jobsContainer.empty();

    jobsContainer.append(mappedJobs);
};

const initJobSearch = () => {
    const search = $('.bclean-search-input__jobs');

    $(search[0]).on('input', ({ target: { value } }) => 
        initJobs(
            jobsData.filter(({ title }) => title.toLowerCase().includes(value.toLowerCase()))
        )
    );
}

initJobSearch();

initJobs();

initJobsPage();

/*======== TABS START========== */
(function () {
    let tabNav = document.querySelectorAll('.index-types .index-types__tabs-item'),
        tabContent = document.querySelectorAll('.index-types .tab'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabName = this.getAttribute('data-tab-name');

        tabNav.forEach(item => {
            if (item.getAttribute('data-tab-name') === tabName) {
                item.classList.add('is-active')
            } else {
                item.classList.remove('is-active');
            }
        });

        selectTabContent(tabName);
    }

    function selectTabContent() {
        tabContent.forEach(item => {
            if (item.classList.contains(tabName)) {
                item.classList.add('is-active')
                $('.index-types__slider').slick('refresh');
            } else {
                item.classList.remove('is-active')
            }
        })
    }
})();
/*======== TABS END========== */


/*======== SLIDERS START========== */
$('.index-advantages__mobile-slider').slick({
    centerMode: true,
    arrows: false,
    responsive: [{
            breakpoint: 768,
            settings: {
                centerMode: true,
                centerPadding: '60px',
                infinite: true,
                slidesToShow: 1
            }
        }

    ]
});

$('.index-types__slider-residential').slick({
    arrows: true,
    slidesToShow: 4,
    infinite: true,
    prevArrow: false,
    nextArrow: $('.tab-1 .index-types__slider-button'),
    responsive: [
        {
            breakpoint: 992,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                arrows: false,
                centerMode: true,
                variableWidth: true,
                centerPadding: '55px',
                slidesToShow: 1,
            }
        },
    ]
});

$('.index-types__slider-non-residential').slick({
    arrows: true,
    slidesToShow: 4,
    infinite: true,
    prevArrow: false,
    nextArrow: $('.tab-2 .index-types__slider-button'),
    responsive: [{
            breakpoint: 992,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                variableWidth: true,
                centerMode: true,
                nextArrow: false,
                centerPadding: '55px',
                slidesToShow: 1
            }
        }

    ]
});

$('.index-testimonials__slider').slick({
    arrows: true,
    slidesToShow: 3,
    prevArrow: false,
    nextArrow: '.index-testimonials__slider-next',
    responsive: [{
            breakpoint: 1200,
            settings: {
                centerPadding: '60px',
                infinite: true,
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                centerPadding: '120px',
                infinite: true,
                slidesToShow: 3
            }
        },
        {
            breakpoint: 576,
            settings: {
                nextArrow: false,
                centerPadding: '60px',
                infinite: true,
                slidesToScroll: 1,
                centerMode: false,
                arrows: false,
                infinite: false,
                slidesToShow: 1,
                variableWidth: true,
                focusOnSelect: true,
                // variableWidth: true,
                // adaptiveHeight: true,
            }
        }

    ]
});

$('.index-promo__slider-mobile').slick({
    centerMode: false,
    arrows: false,
    infinite: false,
    slidesToShow: 1,
    variableWidth: true,
    focusOnSelect: true,
});

$('.index-clients__slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: false,
    nextArrow: '.index-clients__slider-next',
    variableWidth: true,
    focusOnSelect: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 4,
                infinite: true,
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                nextArrow: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }

    ]
});

/*======== SLIDERS END========== */



/*======== Rating START ========== */
$('.index-testimonials__rating').each((_, el) => {
    const numOfStars = Number(el.dataset['rating']);
    if (!(numOfStars > 0)) {
        return;
    }
    const maxStars = 5;
    const starOutlineTpl = '<span class="bclean-star-outline"></span>'
    const starFillTpl = '<span class="bclean-star"></span>'
    for (let i = 0; i < maxStars; i++) {
        if (i < numOfStars) {
            $(el).append(starFillTpl);
        } else {
            $(el).append(starOutlineTpl);
        }
    }
});

$('.index-testimonials__text').each((_, el) => {
    const text = el.innerHTML;
    const parent = $(el).parent();
    const expand = () => {
        $(el).remove();
        parent.find('.index-testimonials__text_full').css('display', 'block');
    }
    $(el).truncateLines({
        appendHtml: `<br /> <button class="expand-btn">Читать целиком</button>`,
        lines: 7
    });
    parent.append(`
        <p class="index-testimonials__text index-testimonials__text_full" style="display: none">
            ${text}
        </p>
    `);
    $(el).find('.expand-btn').on('click', (e) => {
        e.stopPropagation();
        expand();
    });
})
/*======== Rating END ========== */


/*======== Index slider click START ========== */
$('.index-types__slider-item').on('click', (e) => {
    const item = $(e.target).closest('.index-types__slider-item');
    if (item.data('cleaningType') !== undefined) {
        window.location.assign('/calculator.html#' + 'cleaningType=' + item.data('cleaningType'));
    }
});
/*======== Index slider click END ========== */

$('.index-promo__reffering-form-copy-btn').on('click', (e) => {
    e.preventDefault();
    var copyText = $(".index-promo__reffering-form-input")[0];
    copyText.select();
    document.execCommand("copy");

    const tooltip = $('.index-promo__reffering-form-content-signed-in bclean-tooltip')[0];
    tooltip.style.visibility = "visible";
    tooltip.style.opacity = "1";
    setTimeout(() => {
        tooltip.style.visibility = "hidden"
        tooltip.style.opacity = "0";
    }, 2500);
});

// ADD SERVICE LIST:
const list1 = [
    {
        label: 'Почасовая оплата',
        price: 200,
    },
    {
        label: 'Уборка кухни',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Глажка белья',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка на балконе/лоджии',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Чистка микроволновки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Стирка белья',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мытьё холодильника',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Уборка внутри кухонных шкафов',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Мытьё кухонной вытяжки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Мытьё посуды',
        price: 200,
        group: 'Кухня',
    },
];
const list2 = [
    {
        label: 'Чистка духовки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Отмывка и химчистка поверхностей',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка комнаты',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка ванной комнаты',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка гостинной',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка детской',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка коридора',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мойка зеркал',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мытье окон',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Мытье окон на лоджии',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Мытье нестандартных окон',
        price: 200,
        group: 'Балкон/лоджия',
    },
];

function makeFormCalculatorAddServiceLists() {

    const uls = [list1, list2].map((list, listId) => {
        const ul = document.createElement('ul');
        ul.classList.add('add-service-form__list-' + (listId+1))

        list.forEach((item, itemId) => {
            const id = 'add-service-form-' + listId + itemId

            const label = document.createElement('label');
            label.classList.add('add-service__label');
            label.setAttribute('for', id);
            label.textContent = item.label + '    ' + '+' + item.price + '‎₽';

            const checkbox = document.createElement('input');
            checkbox.setAttribute('id', id);
            checkbox.setAttribute('type', 'checkbox');
            checkbox.classList.add('add-service__checkbox');

            const priceSpan = document.createElement('span')
            priceSpan.classList.add('add-service__price');
            priceSpan.innerText = '+' + item.price + 'Р';

            const li = document.createElement('li');
            li.appendChild(checkbox);
            li.appendChild(label);
            if (listId === 0 && itemId === 0) {
                $(li).append(`
                    <select 
                        name="hours" 
                        id="add-service-hours-${itemId}" 
                        class="custom-select sources"
                        placeholder=""
                        value="1"
                    >
                        <option selected value="1">1 час</option>
                        <option value="2">2 часа</option>
                        <option value="3">3 часа</option>
                        <option value="4">4 часа</option>
                        <option value="5+">5 и более</option>
                    </select>
                `)
            }
            // li.appendChild(priceSpan);

            ul.appendChild(li);
        })
        return ul;
    });

    return uls;

}

const addServiceBlock = $('.calc-form-add-service-desktop');
addServiceBlock && addServiceBlock.append(makeFormCalculatorAddServiceLists);



// ADD SERVICE LIST MOBILE:
function makeFormCalculatorAddServiceListsMobile() {

    const containers = {
        'Кухни': null,
        'Комнаты': null,
        'Балкон/Лоджия': null,
    }

    const listIds = ['kitchen', 'rooms', 'balcony'];

    ['Кухня', 'Комнаты', 'Балкон/лоджия'].forEach((g, i) => {
        const heading = document.createElement('h2');
        heading.innerText = g;
        const ul = document.createElement('ul');
        ul.classList.add('add-service-form__list-mobile');
        const container = document.createElement('div');
        container.classList.add('add-service-mobile-group');
        container.appendChild(heading);
        container.appendChild(ul);
        fillList(ul, g);
        containers[g] = container;
    });

    function fillList (ul, groupId) {
        const list = [...list1, ...list2].forEach((item, itemId) => {
    
            if (item.group !== groupId) {
                return;
            }

            const id = 'add-service-form-mobile-' + itemId
    
            const label = document.createElement('label');
            label.classList.add('add-service__label');
            label.setAttribute('for', id);
            label.textContent = item.label + '    ' + '+' + item.price + '‎₽';
    
            const checkbox = document.createElement('input');
            checkbox.setAttribute('id', id);
            checkbox.setAttribute('type', 'checkbox');
            checkbox.setAttribute('hidden', 'true');
            checkbox.classList.add('add-service__checkbox');
    
            const priceSpan = document.createElement('span')
            priceSpan.classList.add('add-service__price');
            priceSpan.innerText = '+' + item.price + 'Р';
    
            const li = document.createElement('li');
            li.appendChild(checkbox);
            li.appendChild(label);
            // li.appendChild(priceSpan);
    
            ul.appendChild(li);
    
        })
    }

    const makeFirstItem = () => {
        return $.parseHTML(`
            <li class="add-service-form-mobile-main-item">
                <input id="add-service-form-mobile-main" type="checkbox" checked class="add-service__checkbox">
                <label class="add-service__label" for="add-service-mobile-main">Почасовая оплата</label>
                <select 
                    name="hours" 
                    id="add-service-hours" 
                    class="custom-select sources"
                    placeholder=""
                    value="1"
                >
                    <option selected value="1">1 час</option>
                    <option value="2">2 часа</option>
                    <option value="3">3 часа</option>
                    <option value="4">4 часа</option>
                    <option value="5+">5 и более</option>
                </select>
                <span class="add-service__price">1200Р</span>
            </li>
        `)
    }

    const res = document.createElement('div');
    res.classList.add('add-service-mobile-groups-container')
    $(res)
        .append(makeFirstItem())
        .append(['Кухня', 'Комнаты', 'Балкон/лоджия'].map(g => containers[g]))
    return res;

}

addServiceBlockMobile = $('.calc-form-add-service-mobile');
addServiceBlockMobile && addServiceBlockMobile.append(makeFormCalculatorAddServiceListsMobile);



// SWITCH TABS:
$('.residence-type-switch__btn').on('click', (e) => {
    e.preventDefault();
    $('.residence-type-switch__btn').removeClass('is-active');
    $(e.target).closest('button').addClass('is-active');
    if (e.target.closest('button').dataset.type === 'residential') {
        $('.calculator-page').removeClass('non-residential');
        $('.calculator-page').addClass('residential');
    }
    if (e.target.closest('button').dataset.type === 'non-residential') {
        $('.calculator-page').removeClass('residential');
        $('.calculator-page').addClass('non-residential');
    }
})

// Switch repeat number
$('.calc-form-repeat__select .calc-form-repeat__select-item').on('click', (e) => {
    $('.calc-form-repeat__select .calc-form-repeat__select-item').removeClass('selected');
    $(e.target).closest('.calc-form-repeat__select-item').addClass('selected');
    const value = $(e.target).closest('.calc-form-repeat__select-item').data('value');
    $(e.target).closest('.calc-form-repeat__select').find('.calc-form-repeat__select-input').attr('value', value);

    if (value === 1) {
        $('.calc-form-repeat .orders__cleaners-container')[0].classList.add('orders__cleaners-container_hidden');
    } else {
        $('.calc-form-repeat .orders__cleaners-container')[0].classList.remove('orders__cleaners-container_hidden');
    }
  
    $('.calc-form-cleaner').hide();
});

/**
 * Switch list
 */
$('.cleaning-type-switch__list').on('click', (e) => {
    e.target.classList.toggle('is-selected');
});


/**
 * Construct choose cleaner table
 */
const mockCleaners = [
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
]

function createCleanersSelector(cleaners) {
    if (!cleaners || !cleaners.length) {
        console.warn('No cleaners provided');
        return;
    }
    const component = $('<div />', {
        class: 'cleaners-selector',
    });
    const list = $('<ul/>', {
        class: 'cleaners-selector-list',
    });
    const topRow = $('<div />', {
        class: 'cleaners-selector-item-selected',
    });
    const firstCleaner = cleaners[0];
    topRow.append(`
    <span class="cleaners-selector-item-selected__name">${firstCleaner.name}</span>
    <p>
        <span class="cleaners-selector-item-selected__rating">${firstCleaner.rating}</span>
        <span class="cleaners-selector-item-selected__rating-star"></span>
        <span class="cleaners-selector-item-selected__rating-label">рейтинг</span>
        <span class="cleaners-selector-item-selected__cleans">${firstCleaner.cleanNum}</span>
        <span class="cleaners-selector-item-selected__cleans-label">уборок</span>
    </p>`);
    component.append(topRow);
    cleaners.forEach(cleaner => {
        const li = $('<li />', {
            class: 'cleaners-selector-item',
        });
        li.append(`
        <span class="cleaners-selector-item__name">${cleaner.name}</span>
        <p>
            <span class="cleaners-selector-item__rating">${cleaner.rating}</span>
            <span class="cleaners-selector-item__rating-star"></span>
            <span class="cleaners-selector-item__rating-label">рейтинг</span>
            <span class="cleaners-selector-item__cleans">${cleaner.cleanNum}</span>
            <span class="cleaners-selector-item__cleans-label">уборок</span>
        </p>`)
        list.append(li);
    });
    component.append(list);
    return component;
}

$('.cleaners-selector-container').append(createCleanersSelector(mockCleaners));


(() => {
    const firstBlock = $('.calculator-form__block-first')[0];
    const secondBlock = $('.calculator-form__block-second')[0];
    const thirdBlock = $('.calculator-form__block-third')[0];
    const fourthBlock = $('.calculator-form__block-fourth')[0];
    const fifthBlock = $('.calculator-form__block-fifth')[0];
    const sixthBlock = $('.calculator-form__block-sixth')[0];

    const list = [...$('.cleaning-type-switch__list li')] || [];

    list.forEach(item => $(item).on('click', () => {
        if (firstBlock) firstBlock.classList.add('calculator-form__block_active');
    }));

    $('#calculator-form-details-area').on('input', () => secondBlock.classList.add('calculator-form__block_active'));
    $('#calculator-form-details-flat').on('input', () => secondBlock.classList.add('calculator-form__block_active'));
    $('.orders__address-date-input').on('blur', () => thirdBlock.classList.add('calculator-form__block_active'));
    $('#calculator-form-contact-phone').on('input', () => fourthBlock.classList.add('calculator-form__block_active'));
    $('.continue-btn').on('click', e => {
        e.preventDefault();
        fifthBlock.classList.add('calculator-form__block_active')
        sixthBlock.classList.add('calculator-form__block_active');
    });

    // setTimeout(() => firstBlock.classList.add('calculator-form__block_active'), 3000);

    // firstBlock.classList.add('calculator-form__block_active')
    // secondBlock.classList.add('calculator-form__block_active');
    // thirdBlock.classList.add('calculator-form__block_active');
    // fourthBlock.classList.add('calculator-form__block_active');
    // fifthBlock.classList.add('calculator-form__block_active');
    // sixthBlock.classList.add('calculator-form__block_active');

})();
/*
 * For tests purpose. Remove on production
 */

$('.header__login-btn').on('click', (e) => {
    e.preventDefault();
    bcleanStore.setValue('user', {firstName: 'Иван', lastName: 'Иванов', id: '123'});
    bcleanStore.setValue('notifications', [
        {title: 'Notification 1', content: 'Content of notification 1'},
        {title: 'Notification 2', content: 'Content of notification 2'},
    ]);
});
// $(".increment .increment__input").on("click", function () {

//   var $button = $(this);
//   var oldValue = $button.parent().find("input").val();

//   if ($button.text() == "+") {
//     var newVal = parseFloat(oldValue) + 1;
//   } else {
//     // Don't allow decrementing below zero
//     if (oldValue > 0) {
//       var newVal = parseFloat(oldValue) - 1;
//     } else {
//       newVal = 0;
//     }
//   }

//   $button.parent().find("input").val(newVal);

// });

window.initIncrements = () => {
  const buttons = $(".increment .increment__btn");

  [...buttons].forEach(button => $(button).on("click", event => {

    event.preventDefault();

    const $button = $(button);
    const $input = $button.parent().find("input")[0];
    const oldValue = $input.value || '1';

    let newVal = 0;

    if ($button.text().includes('+')) {
      newVal = parseFloat(oldValue) + 1;
    } else {
      if (oldValue > 0) {
        newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 0;
      }
    }

    $($input).width(5 + 7 * newVal.toString().length);
    
    $($input).val(newVal);
  }));
};

window.initIncrements();

/*======== SELECT START========== */
function select() {
  let selectFaqsHeader = document.querySelectorAll('.index-faqs__select-header'),
    selectFaqs = document.querySelector('.index-faqs__select'),
    selectFaqsItem = document.querySelectorAll('.index-faqs__select-item');

  selectFaqsHeader.forEach(item => {
    item.addEventListener('click', () => {
      item.parentElement.classList.toggle('is-active');
    })
  });

  selectFaqsItem.forEach(item => {
    item.addEventListener('click', () => {
      let text = item.innerText;
      let currentText = item.closest('.index-faqs__select').querySelector('.index-faqs__select-current');
      currentText.textContent = text;
      selectFaqs.classList.remove('is-active');
    });
  });

};
select();
/*======== SELECT END========== */

window.initCustomSelect = () => {

  $(".custom-select").each(function () {

    var classes = $(this).attr("class"),
      id = $(this).attr("id"),
      name = $(this).attr("name");
    var template = '<div class="' + classes + '">';
    template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
    template += '<div class="custom-options">';
    $(this).find("option").each(function () {
      template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
    });
    template += '</div></div>';

    if (!$(this).parent()[0].classList.contains('custom-select-wrapper')) {
      $(this).wrap('<div class="custom-select-wrapper"></div>');
      $(this).hide();
      $(this).after(template);
    }
  });

  $(".custom-option:first-of-type").hover(function () {
    $(this).parents(".custom-options").addClass("option-hover");
  }, function () {
    $(this).parents(".custom-options").removeClass("option-hover");
  });
  $(".custom-select-trigger").on("click", function () {
    $('html').one('click', function () {
      $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").addClass("opened");
    event.stopPropagation();
  });

  $(".custom-option").on("click touchend", function () {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
  });


  $(".xdsoft_datetimepicker .custom-select-trigger").on("touchend", function () {
    $('html').one('click', function () {
      $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
  });

  $(".xdsoft_datetimepicker .custom-option").on("touchend", function () {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
  });

}

/*=====Custom Select Start ==========*/
$(document).ready(() => {
  window.initCustomSelect();
})


/*=====Custom Select End ==========*/




/*=====reviews Select Start ==========*/
$(".reviews-select").each(function () {
  var classes = $(this).attr("class"),
    id = $(this).attr("id"),
    name = $(this).attr("name");
  var template = '<div class="' + classes + '">';
  template += '<span class="reviews-select-trigger">' + $(this).attr("placeholder") + '</span>';
  template += '<div class="reviews-options">';
  $(this).find("option").each(function () {
    template += '<span class="reviews-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
  });
  template += '</div></div>';

  $(this).wrap('<div class="reviews-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});

$(".reviews-option:first-of-type").hover(function () {
  $(this).parents(".reviews-options").addClass("option-hover");
}, function () {
  $(this).parents(".reviews-options").removeClass("option-hover");
});
$(".reviews-select-trigger").on("click", function () {
  $('html').one('click', function () {
    $(".reviews-select").removeClass("opened");
  });
  $(this).parents(".reviews-select").toggleClass("opened");
  event.stopPropagation();
});

$(".reviews-option").on("click", function () {
  $(this).parents(".reviews-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".reviews-options").find(".reviews-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".reviews-select").removeClass("opened");
  $(this).parents(".reviews-select").find(".reviews-select-trigger").text($(this).text());
});

/*=====reviews Select End ==========*/
const initMenuDropdown = () => {
  const menuList = $('.menu-dropdown');

  [...menuList].forEach(menuItem => {
    const current = $(menuItem).find('.menu-dropdown__current')[0];

    const currentText = $(current)[0].textContent.replace(/\s/g, '').toUpperCase();

    const items = $(menuItem).find('.menu-dropdown__item');

    [...items].forEach(item => {
      if (item.textContent.replace(/\s/g, '').toUpperCase() === currentText) {
        item.classList.add('menu-dropdown__item_active');
      }

      $(item).click(() => {
        if (!item.classList.contains('menu-dropdown__item_active')) {
          [...items].forEach(itemIn => itemIn.classList.remove('menu-dropdown__item_active'));
          item.classList.add('menu-dropdown__item_active');

          $(current).text(item.textContent)
        }
      });
    });

    $(current).click(() => {
      const currentItem = $(menuItem)[0];

      if (currentItem.classList.contains('menu-dropdown_open')) {
        currentItem.classList.remove('menu-dropdown_open');
      } else {
        currentItem.classList.add('menu-dropdown_open');
      }
    })

  });
}

initMenuDropdown();

let datesCount = 2;

const initOrdersNavigation = () => {
  const btns = $('.orders__tab-control-btn');
  const contents = $('.orders__tab-content');

  [...btns].forEach((btn, index) => $(btn).click((e) => {
    const prevIndex = index === 0 ? 1 : 0;

    $(btns[prevIndex]).removeClass('orders__tab-control-btn_active');
    $(contents[prevIndex]).removeClass('orders__tab-content_active');
    $(contents[index]).addClass('orders__tab-content_active');

    $(e.target).addClass('orders__tab-control-btn_active');
  }))
}

const initAddresses = () => {
  const btns = $('.orders__address-item');

  [...btns].forEach(btn => $(btn).on('click', () => {
    [...btns].forEach(btn => btn.classList.remove('orders__address-item_active'));
    btn.classList.add('orders__address-item_active');

    const type = btn.dataset.type;

    if (type === 'living') {
      $('.orders__living').removeClass('d-none');
      $('.orders__non-living').addClass('d-none');
      datesCount = 2;

      initOrdersDate();
    } else {
      $('.orders__living').addClass('d-none');
      $('.orders__non-living').removeClass('d-none');
      datesCount = 1;

      initOrdersDate();
    };
    window.initCalendars();
    window.initCustomSelect();
  }));
}

const initCounts = () => {
  const btns = $('.orders__count');

  [...btns].forEach(btn => $(btn).on('click', () => {
    [...btns].forEach(btn => btn.classList.remove('orders__count_active'));
    btn.classList.add('orders__count_active');
    datesCount = +btn.dataset['count'];


    if (+btn.dataset['count'] === 1) {
      $('.orders .orders__cleaners-container')[0].classList.add('orders__cleaners-container_hidden');
    } else {
      $('.orders .orders__cleaners-container')[0].classList.remove('orders__cleaners-container_hidden');
    }

    initOrdersDate();
    window.initCalendars();
    window.initCustomSelect();
  }));
}

const initOrdersDate = () => {
  const wrapper = $('.calculator-form-details-wrapper');
  let inputs = '';
  for (let index = 0; index < datesCount; index++) {
    inputs += `
      <div class="timepicker-wrapper d-inline-block tooltip tooltip_left tooltip_timepicker">
        <input
          id="calculator-form-details-datetime-${index}"
          autocomplete="off"
          type="text"
          name="datetime"
          class="orders__address-date-input input input_full-border mt-2 calc-form-timepicker-field"
          placeholder="Нажмите, чтобы выбрать дату и время уборки ${index + 1}"
        />
        <div
          class="types-page__hint tooltip__container tooltip__container_cleaner"
        >
          <tooltip
            class="tooltip__content tooltip__content_cleaner"
          >
            <div class="timepicker__component"></div>
          </tooltip>
        </div>
        <div class="timepicker-inline-container d-none d-md-none">
          <div class="timepicker__component"></div>
        </div>
      </div>
    `;
  }
  wrapper.html(inputs);
  initMobileOrders();
}

const initHiddenOrders = () => {
  const packs = [...$('.orders__order-items-wrapper')];

  packs.forEach(pack => {
    const count = $(pack).children().length;

    $(pack).on('click', () => {
      if (!pack.classList.contains('orders__order-items-wrapper_open')) {
        pack.classList.add('orders__order-items-wrapper_open');
        pack.classList.add(`orders__order-items-wrapper_open_${count}`);
      } else {
        pack.classList.remove('orders__order-items-wrapper_open');
        pack.classList.remove(`orders__order-items-wrapper_open_${count}`);
      }
    })
  })
}

const initNavigation = () => {
  const navigationItems = [...$('.orders__navigation a.text')];

  navigationItems.forEach(item => {
    $(item).on('click', () => {
      navigationItems.forEach(item => item.classList.remove('text_active'));
      item.classList.add('text_active');
      $('html, body').animate({
        scrollTop: $('.orders__history').offset().top - 200
      }, 800);
    });
  });

}

const initMobileOrders = () => {
  const pickers = [...$('.timepicker-wrapper.tooltip_timepicker')];
  const containers = [...$('.timepicker-inline-container')];

  pickers.forEach(picker => {
    const input = $(picker).find('.orders__address-date-input');
    const dateWrapper = $(picker).find('.timepicker-inline-container')[0];

    input.on('click', (e) => {
      containers.forEach(container => {
        container.classList.add('d-none');
        container.classList.remove('d-flex');
      });
      dateWrapper.classList.remove('d-none');
      dateWrapper.classList.add('d-flex');
      e.stopPropagation();

      $(document).on('click', () => {
        dateWrapper.classList.remove('d-flex');
        dateWrapper.classList.add('d-none');
      })  
    })
  });

  const field = [...$('.calc-form-timepicker-field')];
}

const initMobileCleanerName = () => {
  const cleaners = [...$('.orders__cleaner-name .orders__cleaner-item')];
  const current = $('.orders__cleaner-name .orders__cleaner-current')[0];

  cleaners.forEach(cleaner => $(cleaner).on('click', () => {
    cleaners.forEach(cleaner => cleaner.classList.remove('orders__cleaner-item_active'));

    cleaner.classList.add('orders__cleaner-item_active');

    $(current).html(cleaner.innerHTML);
  }))

};

initCounts();

initAddresses();

initOrdersNavigation();

initOrdersDate();

initHiddenOrders();

initNavigation();

initMobileOrders();

initMobileCleanerName();

const activateAddresses = () => {
  const addresses = $('.settings__address');

  [...addresses].forEach(address => {
    const live = $(address).find('.settings__address-switch-live');
    const unlive = $(address).find('.settings__address-switch-unlive');
    const unliveBlock = $(address).find('.settings__address-flat-no-living');
    const liveBlock = $(address).find('.settings__address-flat-details');
    const unliveBlockM = $(address).find('.settings__address-form-sub-input_size');
    const liveBlockM = $(address).find('.settings__address-form-sub-input_desktop');

    live.on('click', e => {
      e.preventDefault();

      unliveBlock.addClass('settings__address-flat-no-living_hidden');

      liveBlock.removeClass('settings__address-flat-details_hidden');

      unliveBlockM.addClass('settings__address-flat-no-living_hidden');

      liveBlockM.removeClass('settings__address-flat-no-living_hidden');

      live.addClass('btn_blue-color');
      unlive.removeClass('btn_blue-color');
    });

    unlive.on('click', e => {
      e.preventDefault();
      unliveBlock.removeClass('settings__address-flat-no-living_hidden');

      liveBlock.addClass('settings__address-flat-details_hidden');

      unliveBlockM.removeClass('settings__address-flat-no-living_hidden');

      liveBlockM.addClass('settings__address-flat-no-living_hidden');

      live.removeClass('btn_blue-color');
      unlive.addClass('btn_blue-color');
    });
  })
};

const initAddAddress = () => {
  const btn = $('.settings__add-address');

  const createAddress = (length) => `
    <div class="settings__address mt-3">
      <div class="d-flex justify-content-between">
        <div
          class="settings__address-switch d-flex justify-content-between flex-column flex-md-row"
        >
          <h5 class="hdr hdr_s hdr_very-dark-grey">Адрес ${length}</h5>
          <div class="d-flex mt-2 mt-md-0 ml-n2 ml-md-0">
            <button
              class="settings__address-switch-live btn btn_l btn_blue-color btn_no-background btn_no-uppercase"
            >
              Жилое
            </button>
            <h6 class="hdr hdr_dark-gray hdr_xs hdr_regular">/</h6>
            <button
              class="settings__address-switch-unlive btn btn_l btn_no-background btn_no-uppercase"
            >
              Нежилое
            </button>
          </div>
        </div>

        <button
          class="settings__address-remove btn btn_l btn_no-background btn_no-uppercase"
        >
          Удалить
        </button>
      </div>

      <div class="settings__address-form">
        <form action="">
          <input
            type="text"
            class="settings__address-form-main-input input input_full-border"
            placeholder="Метро"
          />

          <div class="d-flex mt-3">
            <input
              type="text"
              class="settings__address-form-main-input input input_full-border"
              placeholder="Улица, дом, корпус, строение"
            />

            <input
              type="text"
              class="settings__address-form-sub-input settings__address-form-sub-input_size settings__address-flat-no-living_hidden input input_full-border ml-3 d-none d-md-inline-block"
              placeholder="Площадь"
            />

            <input
              type="text"
              class="settings__address-form-sub-input settings__address-form-sub-input_desktop input input_full-border ml-3 d-none d-md-inline-block"
              placeholder="Квартира"
            />
          </div>
        </form>
      </div>

      <div class="settings__address-flat-no-living mt-3 w-100 settings__address-flat-no-living_hidden d-block d-md-none">
        <p class="input-label">Площадь</p>
        <input
          type="text"
          class="settings__address-form-sub-input input input_full-border w-100 mr-2"
          placeholder="М²"
        />
      </div>

      <div
        class="settings__address-flat-details d-flex align-items-md-center align-items-end justify-content-between justify-content-md-start"
      >
        <div class="flex-grow-1 d-inline-block d-md-none">
          <p class="input-label">Квартира</p>
          <input
            type="text"
            class="settings__address-form-sub-input input input_full-border w-100 mr-2"
            placeholder="Квартира"
          />
        </div>
        <div class="settings__address-increment ml-3 ml-md-0">
          <p class="input-label">Комнат</p>
          <div class="increment">
            <button
              class="increment__btn btn btn_l btn_no-background btn_black-color"
            >
              -
            </button>
            <input
              type="text"
              class="increment__input input input_no-border"
              value="1"
            />
            <button
              class="increment__btn btn btn_l btn_no-background btn_black-color"
            >
              +
            </button>
          </div>
        </div>

        <div class="settings__address-increment ml-3">
          <p class="input-label">Санузлов</p>
          <div class="increment">
            <button
              class="increment__btn btn btn_l btn_no-background btn_black-color"
            >
              -
            </button>
            <input
              type="text"
              class="increment__input input input_no-border"
              value="1"
            />
            <button
              class="increment__btn btn btn_l btn_no-background btn_black-color"
            >
              +
            </button>
          </div>
        </div>

        <div class="settings__address-devices mt-4 d-none d-md-block">
          <div class="d-flex">
            
            <div class="settings__address-devices-text text text_s">
              <input id="settings__has-vacuum-${length}" type="checkbox" name="has-vacuum-${length}">
              <label for="settings__has-vacuum-${length}">У меня есть пылесос</label>
            </div>
          </div>

          <div class="d-flex">
            
            <div class="settings__address-devices-text text text_s">
              <input id="settings__has-ladder-${length}" type="checkbox" name="has-ladder-${length}">
              <label for="settings__has-ladder-${length}">У меня есть лестница</label>
            </div>
          </div>
        </div>
      </div>

      <div class="settings__address-comment mt-3 w-100">
        <p class="input-label d-md-none d-inline-block">Комментарий</p>
        <p
          class="input-label input-label_with-padding d-none d-md-inline-block"
        >
          Комментарий
        </p>
        <textarea
          class="textarea"
          placeholder="Комментарий"
          cols="30"
          rows="5"
        ></textarea>
      </div>

      <div class="d-flex mt-2">
        <div class="settings__address-devices-text text text_s">
          <input
            id="settings__default-adr-${length}"
            class="settings__default-adr-default"
            type="checkbox"
            name="default-adr-${length}"
          />
          <label for="settings__default-adr-${length}"
            >Адрес по умолчанию</label
          >
        </div>
      </div>


      <div class="d-block d-md-none mt-2">
        <div class="d-flex align-items-center settings__address-devices-text text text_l mt-1 ml-2">
          <input id="settings__has-vacuum-${length}-mobile" type="checkbox" name="has-vacuum-${length}-mobile" class="settings__has-input" >
          <label for="settings__has-vacuum-${length}-mobile">У меня есть пылесос</label>
        </div>

        <div class="d-flex align-items-center settings__address-devices-text text text_l mt-1 ml-2">
          <input id="settings__has-ladder-${length}-mobile" type="checkbox" name="has-ladder-${length}-mobile" class="settings__has-input" >
          <label for="settings__has-ladder-${length}-mobile">У меня есть стремянка</label>
        </div>
      </div>
    </div>
  `;

  btn.on('click', () => {
    const addresses = $('.settings__accordion .settings__address');

    $(createAddress(addresses.length + 1)).appendTo('.settings__address-container');
    activateAddresses();
    window.initIncrements();
  })
}

const initSettingsPayments = () => {
  const cards = [...$('.settings__card')];

  cards.forEach(card => $(card).on('click', () => {

    const radio = $(card).find('input[type="radio"]');

    cards.forEach(card => $(card).children('.settings__card-item')[0].classList.remove('settings__card-item_active'));

    $(card).children('.settings__card-item')[0].classList.add('settings__card-item_active');

    radio.prop('checked', true);
  }));
}

const initDefaultAddrs = () => {
  const addrss = [...$('.settings__default-adr-default')];

  addrss.forEach(addrs => {
    $(addrs).on('click', () => {
      if ($(addrs).is(':checked')) {
        addrss.forEach(addrs => $(addrs).prop('checked', false));
      }
      $(addrs).prop('checked', true);
    })
  })
}

initAddAddress();

activateAddresses();

initSettingsPayments();

initDefaultAddrs();

const loadedInit = () => {
  const loaders = $('.load-more');

  [...loaders].forEach(load => $(load).on('click', () => {
    if (!$(load).hasClass('loader')) {
      $(load).addClass('loader');

      setTimeout(() => $(load).removeClass('loader'), 1500);
    }
  }));
};

loadedInit();
const cleaners = [
  {
    name: 'Грицко Мария',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Ашмонтас Анна',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Буркивска Инна',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Карташова Ирина',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Калошина Любовь',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Комарова Елена',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Солдатова Людмила',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Солдатова Людмила',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Грицко Мария',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Грицко Мария',
    rating: '4.9',
    cleanCount: '144'
  },
  {
    name: 'Грицко Мария',
    rating: '4.9',
    cleanCount: '144'
  },
];

let currentCleaner = {
  name: 'Грицко Мария',
  rating: '4.9',
  cleanCount: '144'
}

const createCleaner = ({ name, rating, cleanCount }, current = false) => {
  const className = current ? 'cleaner-tooltip-content__current mb-2' : 'cleaner-tooltip-content__item';

  const textColor = current ? 'color-black' : 'color-dark-gray';

  return `
    <div class="${className} d-flex justify-content-between pt-2 pb-1">
      <p class="text text_m text_bold ${textColor}">${name}</p>

      <p class="text text_m ${textColor}">
        <span class='text_bold'>${rating}</span>
        рейтинг
        <span class='text_bold'>${cleanCount}</span>
        уборки
      </p>
    </div>
  `
};

const initCleaners = () => {
  const mappedCleaners = cleaners.map(cleaner => createCleaner(cleaner));

  $('.cleaner-tooltip-content .cleaner-tooltip-content__current').remove();

  const container = $('.cleaner-tooltip-content .cleaner-tooltip-content__container');
  container.before(createCleaner(currentCleaner, true));

  container.empty();
  container.append(mappedCleaners);

  $('.cleaner__input').html(`
    <div class="d-flex justify-content-between w-100">
      <p class="text text_m">
        <span class="text_bold">${currentCleaner.name}</span>
      </p>
      <p>
        <span class="text_bold">${currentCleaner.rating}</span> <span class="text text_m mr-1">рейтинг,</span>
        <span class="text_bold">${currentCleaner.cleanCount}</span> <span class="text text_m">уборки</span>
      </p>
    </div>
  `);

  const cleanersTooltips = $('.cleaner-tooltip-content .cleaner-tooltip-content__container');

  [...cleanersTooltips].forEach(tooltip => {
    const cleanersList = $(tooltip).find('.cleaner-tooltip-content__item');

    [...cleanersList].forEach((cleaner, index) => $(cleaner).on('click', () => {
      currentCleaner = cleaners[index];
      initCleaners();
    }))
  });
};

initCleaners();

const initPaymentMethods = () => {
  const blocks = $(".payment-methods");

  [...blocks].forEach((block) => {
    const methods = $(block).find(".payment-methods__pay-card");

    [...methods].forEach(method => $(method).on("click", () => {
      [...methods].forEach(method => method.classList.remove('payment-methods__pay-card_active'));
      method.classList.add('payment-methods__pay-card_active');
      $('.calc-page__submit')[0].classList.add('calc-page__submit_active');
    }));
  });

  const mainBlock = $('.payment-methods__credit-card')[0];
  
  if (mainBlock !== undefined) {
    $(mainBlock).on('click', () => {
      const mobileCardsBlock = $(mainBlock).find('.payment-methods__mobile-variants')[0];

      mobileCardsBlock.classList.add('payment-methods__mobile-variants_active');

      const mobileCards = [...$(mobileCardsBlock).find('.payment-methods__mobile-variant')];

      mobileCards.forEach(card => $(card).on('click', event => {
        event.stopPropagation();
        $('.payment-method__wrapper').html(card.innerHTML);
        mobileCardsBlock.classList.remove('payment-methods__mobile-variants_active');
      }))
    })
  }
};

const initPaymentsCards = () => {
  const cards = $('.payment-methods__credit-card');

  [...cards].forEach(card => {
    const creditCards = [...$(card).find('.settings__card-item')];

    creditCards.forEach(credit => $(credit).on('click', () => {
      creditCards.forEach(credit => credit.classList.remove('settings__card-item_active'));

      credit.classList.add('settings__card-item_active');

      const wrapper = $(card).find('.payment-method__wrapper');

      wrapper.html(credit.innerHTML);
      
    }));
  });
}

initPaymentMethods();
initPaymentsCards();

(() => {
  const methods = [...$('.deposit__cards-wrapper [data-payment-method]')];

  methods.forEach(method => $(method).on('click', () => {
    methods.forEach(method => {
      method.classList.remove('deposit__card-item_active');
      method.classList.remove('deposit__method-item_active');
    });
    method.classList.add('deposit__card-item_active');
    method.classList.add('deposit__method-item_active');
  }));
})();
(() => {
  let isActive = false;

  const addFakeTooltip = (fakeWrapper, cell) => {
    const tooltipContainer = $(cell).children('.tooltip__container').html();

    const { top } = $(cell).position();

    fakeWrapper.css('top', top + 5);

    fakeWrapper.html(`
      <div class="tooltip__container">
        ${tooltipContainer}
      </div>
    `);
  };

  const removeFakeTooltip = fakeWrapper => fakeWrapper.html('');

  const cells = [...$('.calendar__component .calendar__component-cell-order')];

  cells.forEach(cell => $(cell).on('click', () => {
    const fakeWrapper = $(cell).parents('.calendar__component-cell-wrapper').children('.calendar__component-cell-fake-container');

    if (!cell.classList.contains('tooltip_active') && !cell.classList.contains('calendar__component-cell-order_active')) {
      cells.forEach(inCell => {
        inCell.classList.remove('tooltip_active');
        inCell.classList.remove('calendar__component-cell-order_active');
        $(inCell).children('.calendar__component-cell-arrow')[0].classList.remove('calendar__component-cell-arrow_active');
        $(inCell).children('.calendar__component-cell-arrow')[0].classList.add('calendar__component-cell-arrow');
      });
      if (window.outerHeight < 994) return;
      cell.classList.add('calendar__component-cell-order_active');
      cell.classList.add('tooltip_active');
      $(cell).children('.calendar__component-cell-arrow')[0].classList.add('calendar__component-cell-arrow_active');

      addFakeTooltip(fakeWrapper, cell);
      isActive = true;

    } else {
      cell.classList.remove('tooltip_active');
      cell.classList.remove('calendar__component-cell-order_active');
      $(cell).children('.calendar__component-cell-arrow')[0].classList.remove('calendar__component-cell-arrow_active');
      removeFakeTooltip(fakeWrapper);
      isActive = false;
    }
  }));

  const allCells = [...$('.calendar__component-cell')];

  allCells.forEach(cell => {
    cell.addEventListener('click', () => {
      const calendarTip = $('.calendar__mobile-date-btn.btn.background-color-near-white.mt-2')[0];
      const orderDetails = $('.calendar__mobile-orders-list.mt-3.background-color-near-white')[0];
      allCells.forEach(cell => cell.classList.remove('calendar__component-cell_mobile-active'));
      cell.classList.add('calendar__component-cell_mobile-active');
      calendarTip.classList.remove('d-block');
      calendarTip.classList.add('d-none');
      orderDetails.classList.add('d-block');
      orderDetails.classList.remove('d-none');
    });
  });

  cells.forEach(cell => {
    const arrow = $(cell).find('.calendar__component-cell-arrow')[0];
    const container = $(cell).find('.tooltip__container')[0];
    const fakeWrapper = $(cell).parents('.calendar__component-cell-wrapper').children('.calendar__component-cell-fake-container');

    $(arrow).on('mouseenter', () => {
      if (!isActive) {
        container.classList.add('tooltip__container_visible');
        addFakeTooltip(fakeWrapper, cell);
      }
    });

    $(arrow).on('mouseleave', () => {
      if (!isActive) {
        container.classList.remove('tooltip__container_visible');
        container.classList.add('calendar__component-cell_mobile-active');
        removeFakeTooltip(fakeWrapper);
      }
    });
  });
})();
//=require components/calendar.js
(() => {
  const groups = [...$('.file-input-group')];
  const defaultContent = 'Загрузить файл .doc, .pdf';

  groups.forEach(group => {
    const input = $(group).find('.file-input')[0];
    const content = $(group).find('.file-input__content')[0];
    const remove = $(group).find('.file-input__remove')[0];

    remove.classList.add('file-input__remove_hidden');

    $(input).on('change', ({ currentTarget: { value } }) => {
      if (value !== '' && value !== undefined) {
        const parts = value.split('\\');
        const fileName = parts[parts.length - 1];
        $(content).html(fileName);
        remove.classList.remove('file-input__remove_hidden');
      }
    });

    $(remove).on('click', e => {
      e.preventDefault();
      $(content).html(defaultContent);
      $(input).val('');
      remove.classList.add('file-input__remove_hidden');
    })
  });
})();
window.initCalendars = () => {
  const calendars = [...$(".timepicker-wrapper")];

  calendars.forEach((calendar, index) => {
    const input = $(calendar).find(".calc-form-timepicker-field")[0];
    $(calendar).find('.timepicker__component').datetimepicker({
      timepicker: false,
      inline:true,
      todayButton: false,
      defaultSelect: false,
      onSelectDate: function (value) {
        $(input).val(`${moment(value).format('YYYY-MM-DD')} ${$('.calc-time').val()}`);
      },
    })
    $(input).on('click', e => {
      if (window.outerHeight < 994) return;
      calendars.forEach(calendar => calendar.classList.remove('tooltip_timepicker_active'));
      calendar.classList.add('tooltip_timepicker_active');
      e.stopPropagation();
    })
    $(document).on('click', () => {
      calendar.classList.remove('tooltip_timepicker_active');
    })
  });
  $('.xdsoft_datepicker').append(
    `<div class="xdsoft-bottom-bar">
      <span class="xdsoft-bottom-bar__label">Выберите время: </span>
      <select name="time" class="custom-select sources calc-time" placeholder="08:00">
        <option value="08:00">08:00</option>
        <option value="09:00">09:00</option>
        <option value="10:00">10:00</option>
        <option value="11:00">11:00</option>
        <option value="12:00">12:00</option>
        <option value="13:00">13:00</option>
        <option value="14:00">14:00</option>
        <option value="15:00">15:00</option>
        <option value="16:00">16:00</option>
        <option value="17:00">17:00</option>
        <option value="18:00">18:00</option>
        <option value="19:00">19:00</option>
        <option value="20:00">20:00</option>
      </select>
    </div>`
  );

  [].forEach.call($('.calc-form-timepicker-field'), item => $(item).on('keypress', () => false));
  $.datetimepicker.setLocale('ru');
};

window.initCalendars();
//# sourceMappingURL=main.js.map
