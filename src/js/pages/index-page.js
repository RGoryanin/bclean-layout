/*======== TABS START========== */
(function () {
    let tabNav = document.querySelectorAll('.index-types .index-types__tabs-item'),
        tabContent = document.querySelectorAll('.index-types .tab'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabName = this.getAttribute('data-tab-name');

        tabNav.forEach(item => {
            if (item.getAttribute('data-tab-name') === tabName) {
                item.classList.add('is-active')
            } else {
                item.classList.remove('is-active');
            }
        });

        console.log(tabName);
        selectTabContent(tabName);
    }

    function selectTabContent() {
        tabContent.forEach(item => {
            if (item.classList.contains(tabName)) {
                item.classList.add('is-active')
                $('.index-types__slider').slick('refresh');
            } else {
                item.classList.remove('is-active')
            }
        })
    }
})();
/*======== TABS END========== */


/*======== SLIDERS START========== */
$('.index-advantages__mobile-slider').slick({
    centerMode: true,
    arrows: false,
    responsive: [{
            breakpoint: 768,
            settings: {
                centerMode: true,
                centerPadding: '60px',
                infinite: true,
                slidesToShow: 1
            }
        }

    ]
});

$('.index-types__slider-residential').slick({
    arrows: true,
    slidesToShow: 4,
    infinite: true,
    prevArrow: false,
    nextArrow: $('.tab-1 .index-types__slider-button'),
    responsive: [
        {
            breakpoint: 992,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                arrows: false,
                centerMode: true,
                variableWidth: true,
                centerPadding: '55px',
                slidesToShow: 1,
            }
        },
    ]
});

$('.index-types__slider-non-residential').slick({
    arrows: true,
    slidesToShow: 4,
    infinite: true,
    prevArrow: false,
    nextArrow: $('.tab-2 .index-types__slider-button'),
    responsive: [{
            breakpoint: 992,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                centerPadding: '60px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                variableWidth: true,
                centerMode: true,
                nextArrow: false,
                centerPadding: '55px',
                slidesToShow: 1
            }
        }

    ]
});

$('.index-testimonials__slider').slick({
    arrows: true,
    slidesToShow: 3,
    prevArrow: false,
    nextArrow: '.index-testimonials__slider-next',
    responsive: [{
            breakpoint: 1200,
            settings: {
                centerPadding: '60px',
                infinite: true,
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                centerPadding: '20px',
                infinite: true,
                slidesToShow: 3
            }
        },
        {
            breakpoint: 576,
            settings: {
                nextArrow: false,
                centerMode: true,
                centerPadding: '60px',
                infinite: true,
                slidesToShow: 1,
                focusOnSelect: true,
                slidesToScroll: 1,
                variableWidth: true,
                // variableWidth: true,
                // adaptiveHeight: true,
            }
        }

    ]
});

$('.index-promo__slider-mobile').slick({
    centerMode: false,
    arrows: false,
    infinite: false,
    slidesToShow: 1,
    variableWidth: true,
    focusOnSelect: true,
});

$('.index-clients__slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: false,
    nextArrow: '.index-clients__slider-next',
    variableWidth: true,
    focusOnSelect: true,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 4,
                infinite: true,
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                nextArrow: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }

    ]
});

/*======== SLIDERS END========== */



/*======== Rating START ========== */
$('.index-testimonials__rating').each((_, el) => {
    const numOfStars = Number(el.dataset['rating']);
    if (!(numOfStars > 0)) {
        return;
    }
    const maxStars = 5;
    const starOutlineTpl = '<span class="bclean-star-outline"></span>'
    const starFillTpl = '<span class="bclean-star"></span>'
    for (let i = 0; i < maxStars; i++) {
        if (i < numOfStars) {
            $(el).append(starFillTpl);
        } else {
            $(el).append(starOutlineTpl);
        }
    }
});

$('.index-testimonials__text').each((_, el) => {
    const text = el.innerHTML;
    const parent = $(el).parent();
    const expand = () => {
        $(el).remove();
        parent.find('.index-testimonials__text_full').css('display', 'block');
    }
    $(el).truncateLines({
        appendHtml: `<button class="index-testimonials__expand-btn">Читать целиком</button>`,
    });
    parent.append(`
        <p class="index-testimonials__text index-testimonials__text_full" style="display: none">
            ${text}
        </p>
    `);
    $(el).find('.index-testimonials__expand-btn').on('click', expand);
})
/*======== Rating END ========== */


/*======== Index slider click START ========== */
$('.index-types__slider-item').on('click', (e) => {
    const item = $(e.target).closest('.index-types__slider-item');
    if (item.data('cleaningType') !== undefined) {
        window.location.assign('/calculator.html#' + 'cleaningType=' + item.data('cleaningType'));
    }
});
/*======== Index slider click END ========== */

$('.index-promo__reffering-form-copy-btn').on('click', (e) => {
    e.preventDefault();
    var copyText = $(".index-promo__reffering-form-input")[0];
    copyText.select();
    document.execCommand("copy");

    const tooltip = $('.index-promo__reffering-form-content-signed-in bclean-tooltip')[0];
    tooltip.style.visibility = "visible";
    tooltip.style.opacity = "1";
    setTimeout(() => {
        tooltip.style.visibility = "hidden"
        tooltip.style.opacity = "0";
    }, 2500);
});
