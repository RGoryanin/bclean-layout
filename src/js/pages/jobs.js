$('.current-vacancies-card').on('click', (e) => {
    const card = e.target.closest('.current-vacancies-card');
    if (card.dataset.page) {
        window.location.href = card.dataset.page;
    }
});

$('.jobs-page__send-cv-btn').on('click', (e) => {
    $('.job-apply-overlay').addClass('is-opened');
    $('.job-apply-modal').addClass('is-opened');
});

$('.job-apply-overlay').on('click', (e) => {
    if (e.target.classList.contains('job-apply-overlay')) {
        $('.job-apply-overlay').removeClass('is-opened');
        $('.job-apply-modal').removeClass('is-opened');
    }
});