/* Tab switching on types page */
(function () {
    var tabNav = document.querySelectorAll('.types-page .types-page__tabs-item'),
        tabContent = document.querySelectorAll('.types-page .types-page__tab-content'),
        tabName;
  
    tabNav.forEach(item => {
      item.addEventListener('click', selectTabNav)
    });
  
    function selectTabNav() {
      tabNav.forEach(item => {
        item.classList.remove('is-active');
      });
      this.classList.add('is-active');
  
      tabName = this.getAttribute('data-tab-name');
      selectTabContent(tabName);
    }
  
    function selectTabContent() {
      tabContent.forEach(item => {
        if (item.dataset.tabId === tabName) {
          item.classList.add('is-active')
        } else {
          item.classList.remove('is-active')
        }
      })
    }
  })();