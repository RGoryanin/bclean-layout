// ADD SERVICE LIST:
const list1 = [
    {
        label: 'Почасовая оплата',
        price: 200,
    },
    {
        label: 'Уборка кухни',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Глажка белья',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка на балконе/лоджии',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Чистка микроволновки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Стирка белья',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мытьё холодильника',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Уборка внутри кухонных шкафов',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Мытьё кухонной вытяжки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Мытьё посуды',
        price: 200,
        group: 'Кухня',
    },
];
const list2 = [
    {
        label: 'Чистка духовки',
        price: 200,
        group: 'Кухня',
    },
    {
        label: 'Отмывка и химчистка поверхностей',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка комнаты',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка ванной комнаты',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка гостинной',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка детской',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Уборка коридора',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мойка зеркал',
        price: 200,
        group: 'Комнаты',
    },
    {
        label: 'Мытье окон',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Мытье окон на лоджии',
        price: 200,
        group: 'Балкон/лоджия',
    },
    {
        label: 'Мытье нестандартных окон',
        price: 200,
        group: 'Балкон/лоджия',
    },
];

function makeFormCalculatorAddServiceLists() {

    const uls = [list1, list2].map((list, listId) => {
        const ul = document.createElement('ul');
        ul.classList.add('add-service-form__list-' + (listId+1))

        list.forEach((item, itemId) => {
            const id = 'add-service-form-' + listId + itemId

            const label = document.createElement('label');
            label.classList.add('add-service__label');
            label.setAttribute('for', id);
            label.textContent = item.label + '    ' + '+' + item.price + '‎₽';

            const checkbox = document.createElement('input');
            checkbox.setAttribute('id', id);
            checkbox.setAttribute('type', 'checkbox');
            checkbox.classList.add('add-service__checkbox');

            const priceSpan = document.createElement('span')
            priceSpan.classList.add('add-service__price');
            priceSpan.innerText = '+' + item.price + 'Р';

            const li = document.createElement('li');
            li.appendChild(checkbox);
            li.appendChild(label);
            if (listId === 0 && itemId === 0) {
                $(li).append(`
                    <select 
                        name="hours" 
                        id="add-service-hours" 
                        class="custom-select sources"
                        placeholder=""
                        value="1"
                    >
                        <option selected value="1">1 час</option>
                        <option value="2">2 часа</option>
                        <option value="3">3 часа</option>
                        <option value="4">4 часа</option>
                        <option value="5+">5 и более</option>
                    </select>
                `)
            }
            // li.appendChild(priceSpan);

            ul.appendChild(li);
        })
        return ul;
    });

    return uls;

}

const addServiceBlock = $('.calc-form-add-service-desktop');
addServiceBlock && addServiceBlock.append(makeFormCalculatorAddServiceLists);



// ADD SERVICE LIST MOBILE:
function makeFormCalculatorAddServiceListsMobile() {

    const containers = {
        'Кухни': null,
        'Комнаты': null,
        'Балкон/Лоджия': null,
    }

    const listIds = ['kitchen', 'rooms', 'balcony'];

    ['Кухня', 'Комнаты', 'Балкон/лоджия'].forEach((g, i) => {
        const heading = document.createElement('h2');
        heading.innerText = g;
        const ul = document.createElement('ul');
        ul.classList.add('add-service-form__list-mobile');
        const container = document.createElement('div');
        container.classList.add('add-service-mobile-group');
        container.appendChild(heading);
        container.appendChild(ul);
        fillList(ul, g);
        containers[g] = container;
    });

    function fillList (ul, groupId) {
        const list = [...list1, ...list2].forEach((item, itemId) => {
    
            if (item.group !== groupId) {
                return;
            }

            const id = 'add-service-form-mobile-' + itemId
    
            const label = document.createElement('label');
            label.classList.add('add-service__label');
            label.setAttribute('for', id);
            label.textContent = item.label + '    ' + '+' + item.price + '‎₽';
    
            const checkbox = document.createElement('input');
            checkbox.setAttribute('id', id);
            checkbox.setAttribute('type', 'checkbox');
            checkbox.setAttribute('hidden', 'true');
            checkbox.classList.add('add-service__checkbox');
    
            const priceSpan = document.createElement('span')
            priceSpan.classList.add('add-service__price');
            priceSpan.innerText = '+' + item.price + 'Р';
    
            const li = document.createElement('li');
            li.appendChild(checkbox);
            li.appendChild(label);
            // li.appendChild(priceSpan);
    
            ul.appendChild(li);
    
        })
    }

    const makeFirstItem = () => {
        return $.parseHTML(`
            <li class="add-service-form-mobile-main-item">
                <input id="add-service-form-mobile-main" type="checkbox" checked class="add-service__checkbox">
                <label class="add-service__label" for="add-service-mobile-main">Почасовая оплата</label>
                <select 
                    name="hours" 
                    id="add-service-hours" 
                    class="custom-select sources"
                    placeholder=""
                    value="1"
                >
                    <option selected value="1">1 час</option>
                    <option value="2">2 часа</option>
                    <option value="3">3 часа</option>
                    <option value="4">4 часа</option>
                    <option value="5+">5 и более</option>
                </select>
                <span class="add-service__price">1200Р</span>
            </li>
        `)
    }

    const res = document.createElement('div');
    res.classList.add('add-service-mobile-groups-container')
    $(res)
        .append(makeFirstItem())
        .append(['Кухня', 'Комнаты', 'Балкон/лоджия'].map(g => containers[g]))
    return res;

}

addServiceBlockMobile = $('.calc-form-add-service-mobile');
addServiceBlockMobile && addServiceBlockMobile.append(makeFormCalculatorAddServiceListsMobile);



// SWITCH TABS:
$('.residence-type-switch__btn').on('click', (e) => {
    e.preventDefault();
    $('.residence-type-switch__btn').removeClass('is-active');
    $(e.target).closest('button').addClass('is-active');
    if (e.target.closest('button').dataset.type === 'residential') {
        $('.calculator-page').removeClass('non-residential');
        $('.calculator-page').addClass('residential');
    }
    if (e.target.closest('button').dataset.type === 'non-residential') {
        $('.calculator-page').removeClass('residential');
        $('.calculator-page').addClass('non-residential');
    }
})

// Switch repeat number
$('.calc-form-repeat__select .calc-form-repeat__select-item').on('click', (e) => {
    $('.calc-form-repeat__select .calc-form-repeat__select-item').removeClass('selected');
    $(e.target).closest('.calc-form-repeat__select-item').addClass('selected');
    const value = $(e.target).closest('.calc-form-repeat__select-item').data('value');
    $(e.target).closest('.calc-form-repeat__select').find('.calc-form-repeat__select-input').attr('value', value);
});

/**
 * Switch list
 */
$('.cleaning-type-switch__list').on('click', (e) => {
    e.target.classList.toggle('is-selected');
});


/**
 * Construct choose cleaner table
 */
const mockCleaners = [
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
    {name: 'Плагина Анастасия', rating: 4.9, cleanNum: 114},
]

function createCleanersSelector(cleaners) {
    if (!cleaners || !cleaners.length) {
        console.warn('No cleaners provided');
        return;
    }
    const component = $('<div />', {
        class: 'cleaners-selector',
    });
    const list = $('<ul/>', {
        class: 'cleaners-selector-list',
    });
    const topRow = $('<div />', {
        class: 'cleaners-selector-item-selected',
    });
    const firstCleaner = cleaners[0];
    topRow.append(`
    <span class="cleaners-selector-item-selected__name">${firstCleaner.name}</span>
    <p>
        <span class="cleaners-selector-item-selected__rating">${firstCleaner.rating}</span>
        <span class="cleaners-selector-item-selected__rating-star"></span>
        <span class="cleaners-selector-item-selected__rating-label">рейтинг</span>
        <span class="cleaners-selector-item-selected__cleans">${firstCleaner.cleanNum}</span>
        <span class="cleaners-selector-item-selected__cleans-label">уборок</span>
    </p>`);
    component.append(topRow);
    cleaners.forEach(cleaner => {
        const li = $('<li />', {
            class: 'cleaners-selector-item',
        });
        li.append(`
        <span class="cleaners-selector-item__name">${cleaner.name}</span>
        <p>
            <span class="cleaners-selector-item__rating">${cleaner.rating}</span>
            <span class="cleaners-selector-item__rating-star"></span>
            <span class="cleaners-selector-item__rating-label">рейтинг</span>
            <span class="cleaners-selector-item__cleans">${cleaner.cleanNum}</span>
            <span class="cleaners-selector-item__cleans-label">уборок</span>
        </p>`)
        list.append(li);
    });
    component.append(list);
    return component;
}

$('.cleaners-selector-container').append(createCleanersSelector(mockCleaners));