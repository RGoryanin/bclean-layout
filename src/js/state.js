function BcleanStore() {

    this.user = {
        id: '123456',
        firstName: 'Иван',
        lastName: 'Иванов',
    };
    this.notifications = [
        {title: 'Notification 1', content: 'Content of notification 1'},
        {title: 'Notification 2', content: 'Content of notification 2'},
    ]
    
    this.setValue = (field, value) => {
        this[field] = value;
        PubSub.publish(field, value)
    }

    this.subscribe = (field, cb) => {
        PubSub.subscribe(field, () => {
            cb(this[field]);
        });
    }

}

window.bcleanStore = new BcleanStore();