// $(".increment .increment__input").on("click", function () {

//   var $button = $(this);
//   var oldValue = $button.parent().find("input").val();

//   if ($button.text() == "+") {
//     var newVal = parseFloat(oldValue) + 1;
//   } else {
//     // Don't allow decrementing below zero
//     if (oldValue > 0) {
//       var newVal = parseFloat(oldValue) - 1;
//     } else {
//       newVal = 0;
//     }
//   }

//   $button.parent().find("input").val(newVal);

// });

const initIncrements = () => {
  const buttons = $(".increment .increment__btn");

  [...buttons].forEach(button => $(button).on("click", event => {

    event.preventDefault();

    const $button = $(button);
    const $input = $button.parent().find("input")[0];
    const oldValue = $input.value || '1';

    let newVal = 0;

    if ($button.text().includes('+')) {
      newVal = parseFloat(oldValue) + 1;
    } else {
      if (oldValue > 0) {
        newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 0;
      }
    }

    $($input).width(5 + 7 * newVal.toString().length);
    
    $($input).val(newVal);
  }));
};

initIncrements();
