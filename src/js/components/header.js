// Header expand
$('.header__burger').on('click', function() {
    $('.header').addClass('header_opened')
})
$('.header__close').on('click', function() {
    $('.header').removeClass('header_opened')
});

// Header accordion
(function() {
    const title = document.querySelectorAll('.header-accordion__item-header');
    const content = document.querySelectorAll('.header-accordion__item-content');

    for(let i = 0; i < title.length; i++) {
        title[i].addEventListener('click', function() {
            if (!(this.classList.contains('active'))) {
                for(let i = 0; i < title.length; i++) {
                    title[i].classList.remove('active')
                }
                this.classList.add('active')
            }
        })
    }
})();

// Header tabs
(function() {
    let tabNav = document.querySelectorAll('.header__tab-btn'),
        tabContent = document.querySelectorAll('.header__tab'),
        tabName;
  
    tabNav.forEach(item => {
      item.addEventListener('click', selectTabNav)
    });
  
    function selectTabNav() {
      tabNav.forEach(item => {
        item.classList.remove('bclean-tabs-vertical__tab_active');
      });
      this.classList.add('bclean-tabs-vertical__tab_active');
  
      tabName = this.getAttribute('data-tab-name');
      console.log(tabName)
      selectTabContent(tabName);
    }
  
    function selectTabContent() {
      tabContent.forEach(item => {
        if (item.dataset.name === tabName) {
          item.classList.add('header__dropdown-content_active')
        } else {
          item.classList.remove('header__dropdown-content_active')
        }
      })
    }
})();

// Header switch content desktop
$('.header__link-residential').on('click', function() {
    if (
        $('.header__dropdown-content[data-name="residential"]').hasClass('header__dropdown-content_active')
        && $('.header').hasClass('header_opened')
    ) {
        $('.header__dropdown-content[data-name="residential"]').removeClass('header__dropdown-content_active');
        $('.header').removeClass('header_opened');
        return;
    }
    $('.header__dropdown').addClass('header__dropdown_active');
    $('.header').addClass('header_opened');
    $('.header__dropdown-content').removeClass('header__dropdown-content_active');
    $('.header__dropdown-content[data-name="residential"]').addClass('header__dropdown-content_active');
});
$('.header__link-non-residential').on('click', function() {
    if (
        $('.header__dropdown-content[data-name="non-residential"]').hasClass('header__dropdown-content_active')
        && $('.header').hasClass('header_opened')
    ) {
        $('.header__dropdown-content[data-name="non-residential"]').removeClass('header__dropdown-content_active');
        $('.header').removeClass('header_opened');
        return;
    }
    $('.header__dropdown').addClass('header__dropdown_active');
    $('.header').addClass('header_opened');
    $('.header__dropdown-content').removeClass('header__dropdown-content_active');
    $('.header__dropdown-content[data-name="non-residential"]').addClass('header__dropdown-content_active');
});
$(document).on('click', function (e) {
    if ($('.header').hasClass('header_opened') && !e.target.closest('.header')) {
        $('.header').removeClass('header_opened');
    }
});

// Header data
$(document).ready(userHandler);
$(document).ready(notifiactionsHandler);

function userHandler() {
    bcleanStore.subscribe('user', (user) => {
        if (!user) {
            $('.header__user-name').text(null);
            $('.header').removeClass('is-logged-in');
            $('body').removeClass('is-logged-in');
        } else {
            $('.header__user-name').text(user.lastName + ' ' + user.firstName);
            $('.header').addClass('is-logged-in');
            $('body').addClass('is-logged-in');
        }
    });
    $('.header__user-name').text(bcleanStore.user.lastName + ' ' + bcleanStore.user.firstName);

    $('.header__logout-btn').on('click', (e) => {
        e.preventDefault();
        bcleanStore.setValue('user', null);
    });
}

function notifiactionsHandler() {
    bcleanStore.subscribe('notifications', (notifications) => {
        if (notifications && notifications.length) {
            $('.header').addClass('has-notifications');
        } else {
            $('.header').removeClass('has-notifications');
        }
    });

    $('.header_notification-btn').on('click', (e) => {
        e.preventDefault();
        // show notifications popup window
    });
}

/**
 * Services and types list items selection
 */
(() => {
    const servicesMap = [
        [0,1,2],
        [1,2,3],
        [3,5,6],
        [1,4,5],
        [0,1,7],
        [3,6,8],
    ];

    $('.header__types-list-residential > li').on('click', (e) => {
        $('.header__types-list-residential').addClass('active');
        $('.header__services-list-residential').addClass('active');
        if (!e.target.classList.contains('.active')) {
            const services = $('.header__services-list-residential li');
            services.removeClass('active');
            $('.header__types-list-residential > li').removeClass('active');
            e.target.classList.add('active');
            const index = $('.header__types-list-residential > li').toArray().indexOf(e.target);
            const servicesIds = servicesMap[index];
            services.each((i, item) => {
                if (servicesIds.includes(i)) {
                    item.classList.add('active');
                }
            });
        }
    });
})();