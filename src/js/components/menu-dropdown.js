const initMenuDropdown = () => {
  const menuList = $('.menu-dropdown');

  [...menuList].forEach(menuItem => {
    const current = $(menuItem).find('.menu-dropdown__current')[0];

    const currentText = $(current)[0].textContent.replace(/\s/g, '').toUpperCase();

    const items = $(menuItem).find('.menu-dropdown__item');

    [...items].forEach(item => {
      if (item.textContent.replace(/\s/g, '').toUpperCase() === currentText) {
        item.classList.add('menu-dropdown__item_active');
      }

      $(item).click(() => {
        if (!item.classList.contains('menu-dropdown__item_active')) {
          [...items].forEach(itemIn => itemIn.classList.remove('menu-dropdown__item_active'));
          item.classList.add('menu-dropdown__item_active');

          $(current).text(item.textContent)
        }
      });
    });

    $(current).click(() => {
      const currentItem = $(menuItem)[0];

      if (currentItem.classList.contains('menu-dropdown_open')) {
        currentItem.classList.remove('menu-dropdown_open');
      } else {
        currentItem.classList.add('menu-dropdown_open');
      }
    })

  });
}

initMenuDropdown();
