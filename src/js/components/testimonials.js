const initClosedTestimonials = () => {
  const testimonials = $(".index-testimonials__block_gray");

  [...testimonials].forEach((testimonial) =>
    $(testimonial).click(() => {
      if (
        $(testimonial).hasClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        )
      ) {
        $(testimonial).removeClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        );
      } else {
        $(testimonial).addClass(
          "index-testimonials__block_gray index-testimonials__block_closed"
        );
      }
    })
  );
};

initClosedTestimonials();
