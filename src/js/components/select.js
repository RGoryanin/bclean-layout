/*======== SELECT START========== */
function select() {
  let selectFaqsHeader = document.querySelectorAll('.index-faqs__select-header'),
      selectFaqs= document.querySelector('.index-faqs__select'),
      selectFaqsItem = document.querySelectorAll('.index-faqs__select-item');

      selectFaqsHeader.forEach( item => {
        item.addEventListener('click', () => {
          item.parentElement.classList.toggle('is-active');
        })
      });

      selectFaqsItem.forEach( item => {
        item.addEventListener('click', () => {
          let text = item.innerText;
          let currentText = item.closest('.index-faqs__select').querySelector('.index-faqs__select-current');
          currentText.textContent = text;
          selectFaqs.classList.remove('is-active');
        });
      });

};
select();
/*======== SELECT END========== */



/*=====Custom Select Start ==========*/
$(document).ready(() => {
  $(".custom-select").each(function() {
    var classes = $(this).attr("class"),
        id      = $(this).attr("id"),
        name    = $(this).attr("name");
    var template =  '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function() {
          template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
    template += '</div></div>';
  
    $(this).wrap('<div class="custom-select-wrapper"></div>');
    $(this).hide();
    $(this).after(template);
  });
  
  $(".custom-option:first-of-type").hover(function() {
    $(this).parents(".custom-options").addClass("option-hover");
  }, function() {
    $(this).parents(".custom-options").removeClass("option-hover");
  });
  $(".custom-select-trigger").on("click", function() {
    $('html').one('click',function() {
      $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
  });
  
  $(".custom-option").on("click", function() {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
  });
})


/*=====Custom Select End ==========*/




/*=====reviews Select Start ==========*/
$(".reviews-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="reviews-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="reviews-options">';
      $(this).find("option").each(function() {
        template += '<span class="reviews-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';

  $(this).wrap('<div class="reviews-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});

$(".reviews-option:first-of-type").hover(function() {
  $(this).parents(".reviews-options").addClass("option-hover");
}, function() {
  $(this).parents(".reviews-options").removeClass("option-hover");
});
$(".reviews-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".reviews-select").removeClass("opened");
  });
  $(this).parents(".reviews-select").toggleClass("opened");
  event.stopPropagation();
});

$(".reviews-option").on("click", function() {
  $(this).parents(".reviews-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".reviews-options").find(".reviews-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".reviews-select").removeClass("opened");
  $(this).parents(".reviews-select").find(".reviews-select-trigger").text($(this).text());
});

/*=====reviews Select End ==========*/