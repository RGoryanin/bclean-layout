const initBars = () => {
  const bars = $('.stars-bar');

  [...bars].forEach(bar => $(bar).stars({ stars: 5 }));
};

initBars();
