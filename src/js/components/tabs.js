/*======== TABS START========== */
let tab = function () {
  let tabNav = document.querySelectorAll('.types__tabs-item'),
      tabContent = document.querySelectorAll('.tab'),
      tabName;

  tabNav.forEach(item => {
    item.addEventListener('click', selectTabNav)
  });

  function selectTabNav() {

    tabNav.forEach(item => {
      item.classList.remove('is-active');
    });
    this.classList.add('is-active');

    tabName = this.getAttribute('data-tab-name');
    console.log(tabName)
    selectTabContent(tabName);
  }

  function selectTabContent() {
    tabContent.forEach(item => {
      if (item.classList.contains(tabName)) {
        $('.types__slider').slick('refresh');
        item.classList.add('is-active')
      } else {
        item.classList.remove('is-active')
      }
    })
  }
};

tab();
/*======== TABS END========== */

// REGISTRATION MODAL TABS

const initModalTabs = () => {
  const regBtn = $('.header__modal-reg-hdr')[0];
  const enterBtn = $('.header__modal-enter-hdr')[0];
  const regContent = $('.header__modal-registration')[0];
  const enterContent = $('.header__modal-enter')[0];

  $(regBtn).click(() => {
    regContent.classList.remove('d-none');
    enterContent.classList.add('d-none');

    regBtn.classList.add('color-blue');
    enterBtn.classList.remove('color-blue');
  });

  $(enterBtn).click(() => {
    enterContent.classList.remove('d-none');
    regContent.classList.add('d-none');

    regBtn.classList.remove('color-blue');
    enterBtn.classList.add('color-blue');
  });
};

initModalTabs();

// REGISTRATION MODAL TABS END
