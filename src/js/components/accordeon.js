/*============= Main Page Accordion Start ===============*/
const title = document.querySelectorAll('.index-faq__item-header');
const content = document.querySelectorAll('.index-faq__item-content');

for (let i = 0; i < title.length; i++) {
  title[i].addEventListener('click', function () {
    if (!(this.classList.contains('active'))) {
      for (let i = 0; i < title.length; i++) {
        title[i].classList.remove('active')
      }
      this.classList.add('active')
    }
  })
}
/* ============== Main Page Accordion End =================*/



/*============= Accordion Start ===============*/
const initAccordion = () => {
  const $accordionsList = $('.accordion');

  [...$accordionsList].forEach(accordions => {
    const $headersList = $(accordions).find('.accordion__header');
    const $itemsList = $(accordions).find('.accordion__item');
    const $contentsList = $(accordions).find('.accordion__content');

    [...$headersList].forEach((header, index) => $(header).click(() => {
      const $item = $itemsList[index];

      if (!($item.classList.contains('accordion__item_active'))) {
        $item.classList.add('accordion__item_active')
      } else {
        $item.classList.remove('accordion__item_active')
      }
    }))

  })
};

initAccordion();

// const settingsTitle = document.querySelectorAll('.settings__accordion-header');
// const settingsContent = document.querySelectorAll('.settings__accordion-content');

// for(let i = 0; i < settingsTitle.length; i++) {
//   settingsTitle[i].addEventListener('click', function() {
//     if (!(this.classList.contains('active'))) {
//       this.classList.add('active')
//     } else {
//       this.classList.remove('active')
//     }
//   })
// }
/* ============== Accordion End =================*/
